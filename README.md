#Attribution

#简介
手机号码归属地是一款根据手机号码查询归属地的软件。根据输入的手机号码，查询手机号码归属省份、归属城市、城市区号、城市邮编、运营商和网络。

#演示
http://www.wandoujia.com/apps/com.mj.attribution

#捐赠
开源，我们是认真的，感谢您对我们开源力量的鼓励。


![支付宝](https://git.oschina.net/uploads/images/2017/0607/164544_ef822cc0_395618.png "感谢您对我们开源力量的鼓励")
![微信](https://git.oschina.net/uploads/images/2017/0607/164843_878b9b7f_395618.png "感谢您对我们开源力量的鼓励")