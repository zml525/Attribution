package com.mj.attribution.service;

import java.io.IOException;
import java.util.Date;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.mj.attribution.bean.ShowjiJSON;
import com.mj.attribution.util.HttpUtils;
import com.mj.attribution.util.RegUtil;

/**
 * 手机号码归属地
 * @author zhaominglei
 * @date 2015-3-10
 * 
 */
public class PhoneNumberService extends BaseService {
	@SuppressWarnings("unused")
	private static final String TAG = PhoneNumberService.class.getSimpleName();
	public static final String SHOWJI_API= "http://v.showji.com/Locating/showji.com20150416273007.aspx"; //手机在线
	
	public ShowjiJSON getAttribution(String phoneNumber) {
		String queryURL = SHOWJI_API+"?m="+HttpUtils.encodeURI(phoneNumber)+"&output=json&callback=querycallback&timestamp="+new Date().getTime();
		String html = HttpUtils.doGetForShowji(phoneNumber, queryURL, null);
		if (html != null && !html.equals("")) {
			String jsonStr = RegUtil.getMatchRegStr(html, "\\((.+?)\\)");
			if (jsonStr != null && !jsonStr.equals("")) {
				try {
					ShowjiJSON showjiJSON = new ObjectMapper().readValue(jsonStr, ShowjiJSON.class);
					return showjiJSON;
				} catch (JsonParseException e) {
					e.printStackTrace();
				} catch (JsonMappingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
}
