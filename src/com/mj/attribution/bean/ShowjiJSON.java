package com.mj.attribution.bean;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
/**
 * 手机号码归属地
 * @author zhaominglei
 * @date 2015-3-10
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ShowjiJSON implements Serializable {
	private static final long serialVersionUID = -5735312045606591201L;
	@JsonProperty("Mobile")
	private String mobile; //所查号码
	@JsonProperty("QueryResult")
	private String queryResult; //是否成功
	@JsonProperty("TO")
	private String to; //运用商
	@JsonProperty("Corp")
	private String corp; //公司
	@JsonProperty("Province")
	private String province; //归属省份
	@JsonProperty("City")
	private String city; //归属城市
	@JsonProperty("AreaCode")
	private String areaCode; //城市区号
	@JsonProperty("PostCode")
	private String postCode; //城市邮编
	@JsonProperty("VNO")
	private String vno; //运用商
	@JsonProperty("Card")
	private String card;
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getQueryResult() {
		return queryResult;
	}
	public void setQueryResult(String queryResult) {
		this.queryResult = queryResult;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getCorp() {
		return corp;
	}
	public void setCorp(String corp) {
		this.corp = corp;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	public String getVno() {
		return vno;
	}
	public void setVno(String vno) {
		this.vno = vno;
	}
	public String getCard() {
		return card;
	}
	public void setCard(String card) {
		this.card = card;
	}
}
